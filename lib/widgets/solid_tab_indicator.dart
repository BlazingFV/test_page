import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// Solid tab bar indicator.
class SolidIndicator extends Decoration {
  @override
  BoxPainter createBoxPainter([VoidCallback? onChanged]) {
    return _SolidIndicatorPainter(this, onChanged!);
  }
}

class _SolidIndicatorPainter extends BoxPainter {
  final SolidIndicator decoration;

  _SolidIndicatorPainter(this.decoration, VoidCallback onChanged)
      : super(onChanged);

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
    assert(configuration.size != null);

    final Rect rect = offset & configuration.size!;
    final Paint paint = Paint();
    paint.color = const Color(0xff81E6D9);
    paint.style = PaintingStyle.fill;
    canvas.drawRect(rect, paint);
  }
}
