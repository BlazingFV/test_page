import 'package:flutter/material.dart';
import 'package:flutter_test_app/views/layouts/desktop_layout.dart';
import 'package:flutter_test_app/views/layouts/mobile_layout.dart';
import 'package:flutter_test_app/views/layouts/responsive_layout.dart';
import 'package:flutter_test_app/views/layouts/tablet_layout.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    
    return const ResponsiveLayout(
      mobileLayout: MobileLayout(),
      desktopLayout: DesktopLayout(),
      tabletLayout: TabletLayout(),
    );
  }
}
