import 'dart:developer';

import 'package:flutter/material.dart';

class ResponsiveLayout extends StatelessWidget {
  final Widget mobileLayout;
  final Widget desktopLayout;
  final Widget tabletLayout;
  const ResponsiveLayout({
    super.key,
    required this.desktopLayout,
    required this.mobileLayout,
    required this.tabletLayout,
  });

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth < 600) {
          log('=========Mobile==========');

          return mobileLayout;
        } else if (constraints.maxWidth >= 600 &&
            constraints.maxWidth <= 1280) {
          log('=========Tablet==========');
          return tabletLayout;
        } else {
          log('=========DeskTop==========');

          return desktopLayout;
        }
      },
    );
  }
}
