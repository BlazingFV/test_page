import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_test_app/views/layouts/tablet/first_tab_screen_tablet.dart';
import 'package:flutter_test_app/views/layouts/widgets/mobile/first_tab_screen_mobile.dart';
import 'package:flutter_test_app/views/layouts/widgets/mobile/second_tab_screen_mobile.dart';
import 'package:flutter_test_app/views/layouts/widgets/mobile/third_tab_screen_mobile.dart';
import 'package:flutter_test_app/views/layouts/widgets/web/first_tab_screen_desktop.dart';
import 'package:flutter_test_app/widgets/my_button.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../widgets/wave_painter.dart';

class TabletLayout extends StatefulWidget {
  const TabletLayout({super.key});

  @override
  State<TabletLayout> createState() => _TabletLayoutState();
}

class _TabletLayoutState extends State<TabletLayout> {
  final ScrollController _scrollController = ScrollController();
  double height = 0;
  double width = 0;
  var buttonOne = GlobalKey();
  var buttonTwo = GlobalKey();
  var buttonThree = GlobalKey();
  int _currentIndex = 0;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        height = MediaQuery.of(context).size.height;
        width = MediaQuery.of(context).size.width;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: ScreenUtil().screenHeight * 0.09,
        backgroundColor: Colors.white,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(12),
            bottomRight: Radius.circular(12),
          ),
        ),
        actions: [
          TextButton(
            onPressed: () {},
            child: Text(
              'Login',
              style: GoogleFonts.lato(
                color: const Color(0xff319795),
                fontSize: 14.sp,
                fontWeight: FontWeight.w600,
                letterSpacing: 0.84.w,
              ),
            ),
          ),
          SizedBox(
            width: ScreenUtil().screenWidth * 0.01,
          )
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView(
              children: [
                ClipPath(
                  clipper: WaveClipper(),
                  child: Container(
                    padding: EdgeInsets.only(top: 0.h),
                    height: ScreenUtil().screenHeight * 0.83,
                    width: ScreenUtil().screenWidth,
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.bottomCenter,
                        end: Alignment.topCenter,
                        // stops: [140],
                        transform: GradientRotation(2.44346),
                        colors: [
                          Color(0xffEBF4FF),
                          Color(0xffE6FFFA),
                        ],
                      ),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Deine Job \nwebsite',
                              textAlign: TextAlign.start,
                              style: GoogleFonts.lato(
                                  fontSize: 65,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 1.26,
                                  height: 1),
                            ),
                            SizedBox(
                              height: 65.h,
                            ),
                            MyElevatedButton(
                              height: ScreenUtil().screenHeight * 0.06,
                              width: ScreenUtil().screenWidth * 0.35,
                              borderRadius: BorderRadius.circular(12),
                              onPressed: () {},
                              child: Text(
                                'Kostenlos Registrieren',
                                style: GoogleFonts.lato(
                                    fontSize: 17,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600,
                                    letterSpacing: 0.84),
                              ),
                            )
                          ],
                        ),
                        Container(
                          clipBehavior: Clip.hardEdge,
                          height: ScreenUtil().screenHeight * 0.54,
                          width: ScreenUtil().screenWidth * 0.54,
                          padding: EdgeInsets.only(top: 5.h, bottom: 5.h),
                          decoration: const BoxDecoration(
                              color: Colors.white, shape: BoxShape.circle),
                          child: SvgPicture.asset(
                            'assets/handscheck.svg',
                            fit: BoxFit.contain,
                            height: ScreenUtil().screenHeight * 0.23,
                            width: ScreenUtil().screenWidth * 0.23,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Center(child: topTabBar()),
                SizedBox(
                  height: ScreenUtil().screenHeight * 0.02,
                ),
                AnimatedSwitcher(
                    duration: const Duration(milliseconds: 500),
                    switchInCurve: Curves.easeIn,
                    switchOutCurve: Curves.easeOut,
                    // reverseDuration: const Duration(milliseconds: 850),
                    transitionBuilder: (child, animation) {
                      return SlideTransition(
                        position: Tween<Offset>(
                                begin: const Offset(1, 0),
                                end: const Offset(0, 0))
                            .animate(animation),
                        child: child,
                      );
                    },
                    layoutBuilder: (currentChild, _) => currentChild!,
                    child: returnCorrectPage()),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget returnCorrectPage() {
    if (_currentIndex == 0) {
      return FirstTabScreenTablet(
        key: const ValueKey<int>(0),
        height: height,
        width: width,
      );
    } else if (_currentIndex == 1) {
      return FirstTabScreenTablet(
        key: const ValueKey<int>(0),
        height: height,
        width: width,
      );
    } else if (_currentIndex == 2) {
      return FirstTabScreenTablet(
        key: const ValueKey<int>(0),
        height: height,
        width: width,
      );
    } else {
      return FirstTabScreenTablet(
        key: const ValueKey<int>(0),
        height: height,
        width: width,
      );
    }
  }

  Widget topTabBar() {
    return SizedBox(
      height: ScreenUtil().screenHeight * 0.09,
      width: ScreenUtil().screenWidth * 0.7,
      // margin: EdgeInsets.only(
      //     left: ScreenUtil().screenWidth * 0.03,
      //     right: ScreenUtil().screenWidth * 0.03),
      child: SingleChildScrollView(
        controller: _scrollController,
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            InkWell(
              onTap: () {
                _scrollController.position.ensureVisible(
                  buttonOne.currentContext!.findRenderObject()!,
                  duration: const Duration(milliseconds: 350),
                  curve: Curves.easeInOut,
                );
                // Scrollable.ensureVisible(

                //   buttonOne.currentContext!,

                // );
                setState(() {
                  _currentIndex = 0;
                });
              },
              child: AnimatedContainer(
                key: buttonOne,
                duration: const Duration(milliseconds: 350),
                curve: Curves.easeInBack,
                height: ScreenUtil().screenHeight * 0.05,
                width: ScreenUtil().screenWidth * 0.23,
                decoration: BoxDecoration(
                  color: _currentIndex == 0
                      ? const Color(0xff81E6D9)
                      : Colors.white,
                  borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(12),
                    topLeft: Radius.circular(12),
                  ),
                  border: Border.all(color: const Color(0xffCBD5E0)),
                ),
                child: Center(
                  child: Text(
                    'Arbeitnehmer',
                    style: GoogleFonts.lato(
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.84,
                      color: _currentIndex == 0
                          ? const Color(0xffE6FFFA)
                          : const Color(0xff319795),
                    ),
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                _scrollController.position.ensureVisible(
                  buttonTwo.currentContext!.findRenderObject()!,
                  duration: const Duration(milliseconds: 350),
                  curve: Curves.easeInOut,
                );
                setState(() {
                  _currentIndex = 1;
                });
              },
              child: AnimatedContainer(
                key: buttonTwo,
                duration: const Duration(milliseconds: 350),
                curve: Curves.easeInBack,
                height: ScreenUtil().screenHeight * 0.05,
                width: ScreenUtil().screenWidth * 0.23,
                decoration: BoxDecoration(
                  color: _currentIndex == 1
                      ? const Color(0xff81E6D9)
                      : Colors.white,
                  border: Border.all(color: const Color(0xffCBD5E0)),
                ),
                child: Center(
                  child: Text(
                    'Arbeitgeber',
                    style: GoogleFonts.lato(
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.84,
                      color: _currentIndex == 1
                          ? const Color(0xffE6FFFA)
                          : const Color(0xff319795),
                    ),
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                _scrollController.position.ensureVisible(
                  buttonThree.currentContext!.findRenderObject()!,
                  duration: const Duration(milliseconds: 350),
                  curve: Curves.easeInOut,
                );
                setState(() {
                  _currentIndex = 2;
                });
              },
              child: AnimatedContainer(
                key: buttonThree,
                duration: const Duration(milliseconds: 350),
                curve: Curves.easeInBack,
                height: ScreenUtil().screenHeight * 0.05,
                width: ScreenUtil().screenWidth * 0.23,
                decoration: BoxDecoration(
                  color: _currentIndex == 2
                      ? const Color(0xff81E6D9)
                      : Colors.white,
                  borderRadius: const BorderRadius.only(
                    bottomRight: Radius.circular(12),
                    topRight: Radius.circular(12),
                  ),
                  border: Border.all(color: const Color(0xffCBD5E0)),
                ),
                child: Center(
                  child: Text(
                    'Temporärbüro',
                    style: GoogleFonts.lato(
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.84,
                      color: _currentIndex == 2
                          ? const Color(0xffE6FFFA)
                          : const Color(0xff319795),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
