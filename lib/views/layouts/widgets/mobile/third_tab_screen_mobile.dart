import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../widgets/wave_painter.dart';

class ThirdTabScreen extends StatelessWidget {
  final double height;
  final double width;
  const ThirdTabScreen({
    super.key,
    required this.height,
    required this.width,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Center(
          child: Text(
            "Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter",
            textAlign: TextAlign.center,
            style: GoogleFonts.lato(
              fontSize: 21,
              fontWeight: FontWeight.w500,
              // letterSpacing: 0.84,
              height: 1.3,
              color: const Color(0xff4A5568),
            ),
          ),
        ),
        SizedBox(
          height: ScreenUtil().screenHeight * 0.02,
        ),
        SizedBox(
          height: ScreenUtil().screenHeight * 0.35,
          child: Stack(
            children: [
              Positioned(
                top: ScreenUtil().screenHeight * 0.06,
                // right: 0,
                left: -ScreenUtil().screenWidth * 0.09,
                bottom: 0,
                child: Container(
                  width: ScreenUtil().screenWidth * 0.508,
                  height: ScreenUtil().screenHeight * 0.508,
                  decoration: BoxDecoration(
                      color: Colors.blueGrey.withOpacity(0.03),
                      backgroundBlendMode: BlendMode.srcOver,
                      shape: BoxShape.circle),
                ),
              ),
              Positioned(
                right: 0,
                top: 0,
                left: ScreenUtil().screenWidth * 0.05,
                bottom: ScreenUtil().screenHeight * 0.16,
                child: FractionallySizedBox(
                  widthFactor: 0.6,
                  heightFactor: 1,
                  child: SvgPicture.asset(
                    'assets/1st_pic_first_tab.svg',
                    fit: BoxFit.contain,
                    // width: ScreenUtil().screenWidth * 0.55,
                  ),
                ),
              ),
              Positioned(
                top: ScreenUtil().screenHeight * 0.09,
                left: ScreenUtil().screenWidth * 0.00,
                bottom: 0,
                right: ScreenUtil().screenWidth * 0.7,
                child: FractionallySizedBox(
                  widthFactor: 0.99,
                  heightFactor: 0.9,
                  child: Text(
                    "1.",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.lato(
                      fontSize: 130.sp,
                      fontWeight: FontWeight.w400,
                      // letterSpacing: 0.84,
                      height: 1.3,
                      color: const Color(0xff718096),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: ScreenUtil().screenHeight * 0.24,
                left: ScreenUtil().screenWidth * 0.34,
                bottom: 0,
                right: 0,
                child: FractionallySizedBox(
                  widthFactor: 1,
                  heightFactor: 0.9,
                  child: Text(
                    "Erstellen dein\nUnternehmensprofil",
                    textAlign: TextAlign.start,
                    style: GoogleFonts.lato(
                      fontSize: 15.75.sp,
                      fontWeight: FontWeight.w500,
                      // letterSpacing: 0.84,
                      height: 1.3,
                      color: const Color(0xff718096),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          // color: Colors.red,
          height: ScreenUtil().screenHeight * 0.47,
          child: Stack(
            children: [
              Positioned(
                  top: 0,
                  right: 0,
                  left: 0,
                  bottom: 0,
                  // bottom: 75,
                  child: RotatedBox(
                    quarterTurns: 2,
                    child: ClipPath(
                      clipper: WaveClipper(),
                      child: Container(
                        height: ScreenUtil().screenHeight * 0.75,
                        width: width,
                        decoration: const BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            colors: [Color(0xFFE6FFFA), Color(0xFFEBF4FF)],
                            stops: [0, 1],
                            tileMode: TileMode.repeated,
                          ),
                        ),
                      ),
                    ),
                  )),
              Positioned(
                top: ScreenUtil().screenHeight * 0.21,
                left: ScreenUtil().screenWidth * 0.21,
                right: 0,
                bottom: 0,
                child: FractionallySizedBox(
                  widthFactor: 0.8,
                  heightFactor: 0.8,
                  child: SvgPicture.asset(
                    'assets/2nd_image_thrd_tab.svg',
                    fit: BoxFit.contain,
                  ),
                ),
              ),
              Positioned(
                top: ScreenUtil().screenHeight * 0.03,
                left: 0,
                bottom: 0,
                right: ScreenUtil().screenWidth * 0.49,
                child: FractionallySizedBox(
                  widthFactor: 0.7,
                  heightFactor: 1,
                  child: Text(
                    "2.",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.lato(
                      fontSize: 130.sp,
                      fontWeight: FontWeight.w400,
                      // letterSpacing: 0.84,
                      height: 1.3,
                      color: const Color(0xff718096),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: ScreenUtil().screenHeight * 0.175,
                left: ScreenUtil().screenWidth * 0.45,
                bottom: 0,
                right: 0,
                child: FractionallySizedBox(
                  widthFactor: 0.95,
                  heightFactor: 0.99,
                  child: Text(
                    "Erhalte Vermittlungs-\nangebot von Arbeitgeber",
                    textAlign: TextAlign.start,
                    style: GoogleFonts.lato(
                      fontSize: 15.75.sp,
                      fontWeight: FontWeight.w500,
                      letterSpacing: 0.47,
                      height: 1.3,
                      color: const Color(0xff718096),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          // color: Colors.red,
          height: ScreenUtil().screenHeight * 0.42,
          child: Stack(
            children: [
              Positioned(
                top: -ScreenUtil().screenHeight * 0.06,
                right: 0,
                left: -ScreenUtil().screenWidth * 0.55,
                bottom: 0,
                child: Container(
                  width: ScreenUtil().screenWidth * 0.85,
                  height: ScreenUtil().screenHeight * 0.708,
                  decoration: BoxDecoration(
                      color: Colors.blueGrey.withOpacity(0.03),
                      backgroundBlendMode: BlendMode.srcOver,
                      shape: BoxShape.circle),
                ),
              ),
              Positioned(
                top: -ScreenUtil().screenHeight * 0.055,
                left: 0,
                bottom: 0,
                right: ScreenUtil().screenWidth * 0.55,
                child: FractionallySizedBox(
                  widthFactor: 0.8,
                  heightFactor: 1,
                  child: Text(
                    "3.",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.lato(
                      fontSize: 130.sp,
                      fontWeight: FontWeight.w400,
                      // letterSpacing: 0.84,
                      height: 1.3,
                      color: const Color(0xff718096),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: ScreenUtil().screenHeight * 0.06,
                left: ScreenUtil().screenWidth * 0.43,
                bottom: 0,
                right: 0,
                child: FractionallySizedBox(
                  widthFactor: 0.95,
                  heightFactor: 0.99,
                  child: Text(
                    "Vermittlung nach\nProvision oder\nStundenlohn",
                    textAlign: TextAlign.start,
                    style: GoogleFonts.lato(
                      fontSize: 15.75.sp,
                      fontWeight: FontWeight.w500,
                      letterSpacing: 0.47,
                      height: 1.3,
                      color: const Color(0xff718096),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: ScreenUtil().screenHeight * 0.15,
                left: ScreenUtil().screenWidth * 0.09,
                bottom: 0,
                right: 0,
                child: FractionallySizedBox(
                  widthFactor: 0.7,
                  heightFactor: 1,
                  child: SvgPicture.asset(
                    'assets/thrd_image_thrd_tab.svg',
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  // Widget firstSvgImageStackWidget() {
  //   return SizedBox(
  //     height: ScreenUtil().screenHeight* 0.35,
  //     child: Stack(
  //       children: [
  //         Positioned(
  //           top: ScreenUtil().screenHeight* 0.06,
  //           // right: 0,
  //           left: -ScreenUtil().screenWidth* 0.09,
  //           bottom: 0,
  //           child: Container(
  //             width: ScreenUtil().screenWidth* 0.508,
  //             height: ScreenUtil().screenHeight* 0.508,
  //             decoration: BoxDecoration(
  //                 color: Colors.blueGrey.withOpacity(0.03),
  //                 backgroundBlendMode: BlendMode.srcOver,
  //                 shape: BoxShape.circle),
  //           ),
  //         ),
  //         Positioned(
  //           right: ScreenUtil().screenWidth* 0.13,
  //           top: ScreenUtil().screenHeight* 0.02,
  //           child: SvgPicture.asset(
  //             'assets/1st_pic_first_tab.svg',
  //             fit: BoxFit.cover,
  //           ),
  //         ),
  //         Positioned(
  //           top: ScreenUtil().screenHeight* 0.09,
  //           left: ScreenUtil().screenWidth* 0.015,
  //           child: Text(
  //             "1.",
  //             textAlign: TextAlign.center,
  //             style: GoogleFonts.lato(
  //               fontSize: 130,
  //               fontWeight: FontWeight.w400,
  //               // letterSpacing: 0.84,
  //               height: 1.3,
  //               color: const Color(0xff718096),
  //             ),
  //           ),
  //         ),
  //         Positioned(
  //           top: ScreenUtil().screenHeight* 0.24,
  //           left: ScreenUtil().screenWidth* 0.33,
  //           child: Text(
  //             "Erstellen dein Lebenslauf",
  //             textAlign: TextAlign.start,
  //             style: GoogleFonts.lato(
  //               fontSize: 15.75,
  //               fontWeight: FontWeight.w400,
  //               // letterSpacing: 0.84,
  //               height: 1.3,
  //               color: const Color(0xff718096),
  //             ),
  //           ),
  //         ),
  //       ],
  //     ),
  //   );
  // }

  // Widget secondSvgImageStackWidget() {
  //   return SizedBox(
  //     // color: Colors.red,
  //     height: ScreenUtil().screenHeight* 0.42,
  //     child: Stack(
  //       children: [
  //         Positioned(
  //             top: 0,
  //             right: 0,
  //             left: 0,
  //             // bottom: 75,
  //             child: RotatedBox(
  //               quarterTurns: 2,
  //               child: ClipPath(
  //                 clipper: WaveClipper(),
  //                 child: Container(
  //                   height: ScreenUtil().screenHeight* 0.75,
  //                   width: width,
  //                   decoration: const BoxDecoration(
  //                     gradient: LinearGradient(
  //                       begin: Alignment.topLeft,
  //                       end: Alignment.bottomRight,
  //                       colors: [Color(0xFFE6FFFA), Color(0xFFEBF4FF)],
  //                       stops: [0, 1],
  //                       tileMode: TileMode.repeated,
  //                     ),
  //                   ),
  //                 ),
  //               ),
  //             )),
  //         Positioned(
  //           top: ScreenUtil().screenHeight* 0.05,
  //           left: ScreenUtil().screenWidth* 0.1,
  //           child: Text(
  //             "2.",
  //             textAlign: TextAlign.center,
  //             style: GoogleFonts.lato(
  //               fontSize: 130,
  //               fontWeight: FontWeight.w400,
  //               // letterSpacing: 0.84,
  //               height: 1.3,
  //               color: const Color(0xff718096),
  //             ),
  //           ),
  //         ),
  //         Positioned(
  //           top: ScreenUtil().screenHeight* 0.198,
  //           left: ScreenUtil().screenWidth* 0.4,
  //           child: Text(
  //             "Erstellen dein Lebenslauf",
  //             textAlign: TextAlign.start,
  //             style: GoogleFonts.lato(
  //               fontSize: 15.75,
  //               fontWeight: FontWeight.w400,
  //               letterSpacing: 0.47,
  //               height: 1.3,
  //               color: const Color(0xff718096),
  //             ),
  //           ),
  //         ),
  //         Positioned(
  //           top: ScreenUtil().screenHeight* 0.25,
  //           left: ScreenUtil().screenWidth* 0.32,
  //           child: SvgPicture.asset(
  //             'assets/2nd_image_first_tab.svg',
  //             fit: BoxFit.cover,
  //           ),
  //         ),
  //       ],
  //     ),
  //   );
  // }

  // Widget thirdSvgImage() {
  //   return SizedBox(
  //     // color: Colors.red,
  //     height: ScreenUtil().screenHeight* 0.42,
  //     child: Stack(
  //       children: [
  //         Positioned(
  //           top: -ScreenUtil().screenHeight* 0.06,
  //           // right: 0,
  //           left: -ScreenUtil().screenWidth* 0.22,
  //           bottom: 0,
  //           child: Container(
  //             width: ScreenUtil().screenWidth* 0.85,
  //             height: ScreenUtil().screenHeight* 0.708,
  //             decoration: BoxDecoration(
  //                 color: Colors.blueGrey.withOpacity(0.03),
  //                 backgroundBlendMode: BlendMode.srcOver,
  //                 shape: BoxShape.circle),
  //           ),
  //         ),
  //         Positioned(
  //           top: -35,
  //           left: ScreenUtil().screenWidth* 0.12,
  //           child: Text(
  //             "3.",
  //             textAlign: TextAlign.center,
  //             style: GoogleFonts.lato(
  //               fontSize: 130,
  //               fontWeight: FontWeight.w400,
  //               // letterSpacing: 0.84,
  //               height: 1.3,
  //               color: const Color(0xff718096),
  //             ),
  //           ),
  //         ),
  //         Positioned(
  //           top: ScreenUtil().screenHeight* 0.04,
  //           left: ScreenUtil().screenWidth* 0.43,
  //           child: Text(
  //             "Mit nur einem Klick\nbewerben",
  //             textAlign: TextAlign.start,
  //             style: GoogleFonts.lato(
  //               fontSize: 15.75,
  //               fontWeight: FontWeight.w400,
  //               letterSpacing: 0.47,
  //               height: 1.3,
  //               color: const Color(0xff718096),
  //             ),
  //           ),
  //         ),
  //         Positioned(
  //           top: ScreenUtil().screenHeight* 0.14,
  //           left: ScreenUtil().screenWidth* 0.24,
  //           child: SvgPicture.asset(
  //             'assets/thrd_image_first_tab.svg',
  //             fit: BoxFit.cover,
  //           ),
  //         ),
  //       ],
  //     ),
  //   );
  // }

}
