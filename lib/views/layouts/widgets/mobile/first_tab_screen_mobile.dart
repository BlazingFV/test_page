import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../widgets/wave_painter.dart';

class FirstTabScreen extends StatelessWidget {
  final double height;
  final double width;
  const FirstTabScreen({
    super.key,
    required this.height,
    required this.width,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Center(
          child: Text(
            "Drei einfache Schritte\nzu deinem neuen Job",
            textAlign: TextAlign.center,
            style: GoogleFonts.lato(
              fontSize: 21.sp,
              fontWeight: FontWeight.w500,
              // letterSpacing: 0.84,
              height: 1.3,
              color: const Color(0xff4A5568),
            ),
          ),
        ),
        SizedBox(
          height: ScreenUtil().screenHeight * 0.02,
        ),
        SizedBox(
          height: ScreenUtil().screenHeight * 0.45,
          width: ScreenUtil().screenWidth * 1,
          child: Stack(
            clipBehavior: Clip.none,
            children: [
              Positioned(
                top: ScreenUtil().screenHeight * 0.07,
                // right: 0,
                left: -ScreenUtil().screenWidth * 0.08,
                bottom: 0,
                child: Container(
                  width: ScreenUtil().screenWidth * 0.508,
                  height: ScreenUtil().screenHeight * 0.508,
                  decoration: BoxDecoration(
                      color: Colors.blueGrey.withOpacity(0.03),
                      backgroundBlendMode: BlendMode.srcOver,
                      shape: BoxShape.circle),
                ),
              ),
              Positioned(
                right: 0,
                top: 0,
                left: ScreenUtil().screenWidth * 0.05,
                bottom: ScreenUtil().screenHeight * 0.17,
                child: FractionallySizedBox(
                  widthFactor: 0.6,
                  heightFactor: 1,
                  child: SvgPicture.asset(
                    'assets/1st_pic_first_tab.svg',
                    fit: BoxFit.contain,
                    // width: ScreenUtil().screenWidth * 0.55,
                  ),
                ),
              ),
              Positioned(
                top: ScreenUtil().screenHeight * 0.13,
                left: ScreenUtil().screenWidth * 0.03,
                child: Text(
                  "1.",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.lato(
                    fontSize: 100.sp,
                    fontWeight: FontWeight.w400,
                    // letterSpacing: 0.84,
                    height: 1.3,
                    color: const Color(0xff718096),
                  ),
                ),
              ),
              Align(
                alignment: const Alignment(0.8, 0.68),
                child: FractionallySizedBox(
                  widthFactor: 0.7,
                  heightFactor: 0.3,
                  child: Text(
                    "Erstellen dein Lebenslauf",
                    textAlign: TextAlign.start,
                    style: GoogleFonts.lato(
                      fontSize: 16.sp,
                      fontWeight: FontWeight.w500,
                      letterSpacing: 0.84,
                      height: 1.3,
                      color: const Color(0xff718096),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          // color: Colors.red,
          height: ScreenUtil().screenHeight * 0.44,
          // width: ScreenUtil().screenWidth * 0.9,

          child: Stack(
            clipBehavior: Clip.none,
            children: [
              Positioned(
                  top: 0,
                  right: 0,
                  left: 0,
                  // bottom: 75,
                  child: RotatedBox(
                    quarterTurns: 2,
                    child: ClipPath(
                      clipper: WaveClipper(),
                      child: Container(
                        height: ScreenUtil().screenHeight * 0.75,
                        // width: ScreenUtil().screenWidth * 1,
                        decoration: const BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            colors: [Color(0xFFE6FFFA), Color(0xFFEBF4FF)],
                            stops: [0, 1],
                            tileMode: TileMode.repeated,
                          ),
                        ),
                      ),
                    ),
                  )),
              Positioned(
                top: ScreenUtil().screenHeight * 0.05,
                left: ScreenUtil().screenWidth * 0.08,
                // bottom: 0,
                // right: 0,
                child: Text(
                  "2.",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.lato(
                    fontSize: 100.sp,
                    fontWeight: FontWeight.w400,
                    // letterSpacing: 0.84,
                    height: 1.3,
                    color: const Color(0xff718096),
                  ),
                ),
              ),
              Positioned(
                top: ScreenUtil().screenHeight * 0.03,
                left: ScreenUtil().screenWidth * 0.18,
                bottom: 0,
                right: 0,
                child: FractionallySizedBox(
                  widthFactor: 0.7,
                  heightFactor: 0.3,
                  child: Text(
                    "Erstellen dein Lebenslauf",
                    textAlign: TextAlign.start,
                    style: GoogleFonts.lato(
                      fontSize: 15.sp,
                      fontWeight: FontWeight.w500,
                      letterSpacing: 0.84,
                      height: 1.3,
                      color: const Color(0xff718096),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: ScreenUtil().screenHeight * 0.21,
                bottom: 0,
                left: ScreenUtil().screenWidth * 0.01,
                right: 0,
                child: FractionallySizedBox(
                  widthFactor: 0.5,
                  heightFactor: 0.8,
                  child: SvgPicture.asset(
                    'assets/2nd_image_first_tab.svg',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          // color: Colors.red,
          height: ScreenUtil().screenHeight * 0.44,
          child: Stack(
            children: [
              Positioned(
                top: -ScreenUtil().screenHeight * 0.09,
                // right: 0,
                left: -ScreenUtil().screenWidth * 0.13,
                bottom: 0,
                child: Container(
                  width: ScreenUtil().screenWidth * 0.608,
                  height: ScreenUtil().screenHeight * 0.608,
                  decoration: BoxDecoration(
                      color: Colors.blueGrey.withOpacity(0.03),
                      backgroundBlendMode: BlendMode.srcOver,
                      shape: BoxShape.circle),
                ),
              ),
              Positioned(
                top: -ScreenUtil().screenHeight * 0.01,
                left: ScreenUtil().screenWidth * 0.08,
                child: Text(
                  "3.",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.lato(
                    fontSize: 100.sp,
                    fontWeight: FontWeight.w400,
                    // letterSpacing: 0.84,
                    height: 1.3,
                    color: const Color(0xff718096),
                  ),
                ),
              ),
              Positioned(
                top: ScreenUtil().screenHeight * 0.08,
                left: ScreenUtil().screenWidth * 0.2,
                bottom: 0,
                right: 0,
                child: FractionallySizedBox(
                  widthFactor: 0.7,
                  heightFactor: 0.9,
                  child: Text(
                    "Mit nur einem Klick\nbewerben",
                    textAlign: TextAlign.start,
                    style: GoogleFonts.lato(
                      fontSize: 15.sp,
                      fontWeight: FontWeight.w500,
                      letterSpacing: 0.47,
                      height: 1.3,
                      color: const Color(0xff718096),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: ScreenUtil().screenHeight * 0.13,
                left: ScreenUtil().screenWidth * 0.05,
                bottom: 0,
                right: 0,
                child: FractionallySizedBox(
                  widthFactor: 0.7,
                  heightFactor: 0.9,
                  child: SvgPicture.asset(
                    'assets/thrd_image_first_tab.svg',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  // Widget firstSvgImageStackWidget() {
  //   return SizedBox(
  //     ScreenUtil().screenScreenUtil().screenHeight: ScreenUtil().screenScreenUtil().screenHeight * 0.35,
  //     child: Stack(
  //       children: [
  //         Positioned(
  //           top: ScreenUtil().screenScreenUtil().screenHeight * 0.06,
  //           // right: 0,
  //           left: -ScreenUtil().screenWidth * 0.09,
  //           bottom: 0,
  //           child: Container(
  //             ScreenUtil().screenWidth: ScreenUtil().screenWidth * 0.508,
  //             ScreenUtil().screenScreenUtil().screenHeight: ScreenUtil().screenScreenUtil().screenHeight * 0.508,
  //             decoration: BoxDecoration(
  //                 color: Colors.blueGrey.withOpacity(0.03),
  //                 backgroundBlendMode: BlendMode.srcOver,
  //                 shape: BoxShape.circle),
  //           ),
  //         ),
  //         Positioned(
  //           right: ScreenUtil().screenWidth * 0.13,
  //           top: ScreenUtil().screenScreenUtil().screenHeight * 0.02,
  //           child: SvgPicture.asset(
  //             'assets/1st_pic_first_tab.svg',
  //             fit: BoxFit.cover,
  //           ),
  //         ),
  //         Positioned(
  //           top: ScreenUtil().screenScreenUtil().screenHeight * 0.09,
  //           left: ScreenUtil().screenWidth * 0.015,
  //           child: Text(
  //             "1.",
  //             textAlign: TextAlign.center,
  //             style: GoogleFonts.lato(
  //               fontSize: 130,
  //               fontWeight: FontWeight.w400,
  //               // letterSpacing: 0.84,
  //               ScreenUtil().screenScreenUtil().screenHeight: 1.3,
  //               color: const Color(0xff718096),
  //             ),
  //           ),
  //         ),
  //         Positioned(
  //           top: ScreenUtil().screenScreenUtil().screenHeight * 0.24,
  //           left: ScreenUtil().screenWidth * 0.33,
  //           child: Text(
  //             "Erstellen dein Lebenslauf",
  //             textAlign: TextAlign.start,
  //             style: GoogleFonts.lato(
  //               fontSize: 15.75,
  //               fontWeight: FontWeight.w400,
  //               // letterSpacing: 0.84,
  //               ScreenUtil().screenScreenUtil().screenHeight: 1.3,
  //               color: const Color(0xff718096),
  //             ),
  //           ),
  //         ),
  //       ],
  //     ),
  //   );
  // }

  // Widget secondSvgImageStackWidget() {
  //   return SizedBox(
  //     // color: Colors.red,
  //     ScreenUtil().screenScreenUtil().screenHeight: ScreenUtil().screenScreenUtil().screenHeight * 0.42,
  //     child: Stack(
  //       children: [
  //         Positioned(
  //             top: 0,
  //             right: 0,
  //             left: 0,
  //             // bottom: 75,
  //             child: RotatedBox(
  //               quarterTurns: 2,
  //               child: ClipPath(
  //                 clipper: WaveClipper(),
  //                 child: Container(
  //                   ScreenUtil().screenScreenUtil().screenHeight: ScreenUtil().screenScreenUtil().screenHeight * 0.75,
  //                   ScreenUtil().screenWidth: ScreenUtil().screenWidth,
  //                   decoration: const BoxDecoration(
  //                     gradient: LinearGradient(
  //                       begin: Alignment.topLeft,
  //                       end: Alignment.bottomRight,
  //                       colors: [Color(0xFFE6FFFA), Color(0xFFEBF4FF)],
  //                       stops: [0, 1],
  //                       tileMode: TileMode.repeated,
  //                     ),
  //                   ),
  //                 ),
  //               ),
  //             )),
  //         Positioned(
  //           top: ScreenUtil().screenScreenUtil().screenHeight * 0.05,
  //           left: ScreenUtil().screenWidth * 0.1,
  //           child: Text(
  //             "2.",
  //             textAlign: TextAlign.center,
  //             style: GoogleFonts.lato(
  //               fontSize: 130,
  //               fontWeight: FontWeight.w400,
  //               // letterSpacing: 0.84,
  //               ScreenUtil().screenScreenUtil().screenHeight: 1.3,
  //               color: const Color(0xff718096),
  //             ),
  //           ),
  //         ),
  //         Positioned(
  //           top: ScreenUtil().screenScreenUtil().screenHeight * 0.198,
  //           left: ScreenUtil().screenWidth * 0.4,
  //           child: Text(
  //             "Erstellen dein Lebenslauf",
  //             textAlign: TextAlign.start,
  //             style: GoogleFonts.lato(
  //               fontSize: 15.75,
  //               fontWeight: FontWeight.w400,
  //               letterSpacing: 0.47,
  //               ScreenUtil().screenScreenUtil().screenHeight: 1.3,
  //               color: const Color(0xff718096),
  //             ),
  //           ),
  //         ),
  //         Positioned(
  //           top: ScreenUtil().screenScreenUtil().screenHeight * 0.25,
  //           left: ScreenUtil().screenWidth * 0.32,
  //           child: SvgPicture.asset(
  //             'assets/2nd_image_first_tab.svg',
  //             fit: BoxFit.cover,
  //           ),
  //         ),
  //       ],
  //     ),
  //   );
  // }

  // Widget thirdSvgImage() {
  //   return SizedBox(
  //     // color: Colors.red,
  //     ScreenUtil().screenScreenUtil().screenHeight: ScreenUtil().screenScreenUtil().screenHeight * 0.42,
  //     child: Stack(
  //       children: [
  //         Positioned(
  //           top: -ScreenUtil().screenScreenUtil().screenHeight * 0.06,
  //           // right: 0,
  //           left: -ScreenUtil().screenWidth * 0.22,
  //           bottom: 0,
  //           child: Container(
  //             ScreenUtil().screenWidth: ScreenUtil().screenWidth * 0.85,
  //             ScreenUtil().screenScreenUtil().screenHeight: ScreenUtil().screenScreenUtil().screenHeight * 0.708,
  //             decoration: BoxDecoration(
  //                 color: Colors.blueGrey.withOpacity(0.03),
  //                 backgroundBlendMode: BlendMode.srcOver,
  //                 shape: BoxShape.circle),
  //           ),
  //         ),
  //         Positioned(
  //           top: -35,
  //           left: ScreenUtil().screenWidth * 0.12,
  //           child: Text(
  //             "3.",
  //             textAlign: TextAlign.center,
  //             style: GoogleFonts.lato(
  //               fontSize: 130,
  //               fontWeight: FontWeight.w400,
  //               // letterSpacing: 0.84,
  //               ScreenUtil().screenScreenUtil().screenHeight: 1.3,
  //               color: const Color(0xff718096),
  //             ),
  //           ),
  //         ),
  //         Positioned(
  //           top: ScreenUtil().screenScreenUtil().screenHeight * 0.04,
  //           left: ScreenUtil().screenWidth * 0.43,
  //           child: Text(
  //             "Mit nur einem Klick\nbewerben",
  //             textAlign: TextAlign.start,
  //             style: GoogleFonts.lato(
  //               fontSize: 15.75,
  //               fontWeight: FontWeight.w400,
  //               letterSpacing: 0.47,
  //               ScreenUtil().screenScreenUtil().screenHeight: 1.3,
  //               color: const Color(0xff718096),
  //             ),
  //           ),
  //         ),
  //         Positioned(
  //           top: ScreenUtil().screenScreenUtil().screenHeight * 0.14,
  //           left: ScreenUtil().screenWidth * 0.24,
  //           child: SvgPicture.asset(
  //             'assets/thrd_image_first_tab.svg',
  //             fit: BoxFit.cover,
  //           ),
  //         ),
  //       ],
  //     ),
  //   );
  // }

}
