import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../widgets/wave_painter.dart';

class FirstTabScreenDesktop extends StatelessWidget {
  final double height;
  final double width;
  const FirstTabScreenDesktop({
    super.key,
    required this.height,
    required this.width,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Center(
          child: Text(
            "Drei einfache Schritte\nzu deinem neuen Job",
            textAlign: TextAlign.center,
            style: GoogleFonts.lato(
              fontSize: 30.sp,
              fontWeight: FontWeight.w500,
              letterSpacing: 0.94,
              // height: 1.3.h,
              color: const Color(0xff4A5568),
            ),
          ),
        ),
        firstStackedImage(),
        SizedBox(
          // color: Colors.red,
          height: ScreenUtil().screenHeight * 0.72,
          child: Stack(
            children: [
              secondStackedImage(),
              Positioned(
                top: -ScreenUtil().screenHeight * 0.19,
                left: 60.w,
                // bottom: -50,
                child: SvgPicture.asset(
                  'assets/arrow_1.svg',
                  fit: BoxFit.cover,
                  width: 583.26,
                ),
              ),
              Positioned(
                // top: -50,
                left: ScreenUtil().screenWidth * 0.40,
                bottom: 0,
                child: SvgPicture.asset(
                  'assets/arrow_2.svg',
                  fit: BoxFit.cover,
                  width: 530.26,
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: ScreenUtil().screenHeight * 0.72,
          child: Stack(
            clipBehavior: Clip.hardEdge,
            children: [
              thirdStackedImage(),
            ],
          ),
        ),
      ],
    );
  }

  SizedBox thirdStackedImage() {
    return SizedBox(
      // color: Colors.red,
      height: ScreenUtil().screenHeight * 0.55,
      child: Stack(
        children: [
          Positioned(
            top: -ScreenUtil().screenHeight / 13,
            // right: 0,
            left: ScreenUtil().screenWidth * 0.24,
            bottom: 0,
            child: Container(
              width: ScreenUtil().screenWidth * 0.25,
              height: ScreenUtil().screenHeight * 0.25,
              decoration: BoxDecoration(
                  color: Colors.blueGrey.withOpacity(0.03),
                  backgroundBlendMode: BlendMode.srcOver,
                  shape: BoxShape.circle),
            ),
          ),
          Positioned(
            top: -ScreenUtil().screenHeight * 0.01,
            left: ScreenUtil().screenWidth * 0.34,
            child: Text(
              "3.",
              textAlign: TextAlign.center,
              style: GoogleFonts.lato(
                fontSize: 130,
                fontWeight: FontWeight.w400,
                // letterSpacing: 0.84,
                height: 1.3,
                color: const Color(0xff718096),
              ),
            ),
          ),
          Positioned(
            top: ScreenUtil().screenHeight / 8,
            left: ScreenUtil().screenWidth / 2.3,
            child: Text(
              "Mit nur einem Klick\nbewerben",
              textAlign: TextAlign.start,
              style: GoogleFonts.lato(
                fontSize: 15.75.sp,
                fontWeight: FontWeight.w600,
                letterSpacing: 0.47,
                height: 1.3,
                color: const Color(0xff718096),
              ),
            ),
          ),
          Positioned(
            top: ScreenUtil().screenHeight * 0.05,
            left: ScreenUtil().screenWidth * 0.65,
            child: SvgPicture.asset(
              'assets/thrd_image_first_tab.svg',
              fit: BoxFit.cover,
              width: ScreenUtil().screenWidth * 0.35,
            ),
          ),
        ],
      ),
    );
  }

  SizedBox firstStackedImage() {
    return SizedBox(
      height: ScreenUtil().screenHeight * 0.55,
      child: Stack(
        children: [
          Positioned(
            top: ScreenUtil().screenHeight / 4,
            // right: 0,
            left: ScreenUtil().screenWidth * 0.01,
            bottom: 0,
            child: Container(
              width: ScreenUtil().screenWidth * 0.35,
              height: ScreenUtil().screenHeight * 0.35,
              decoration: BoxDecoration(
                  color: Colors.blueGrey.withOpacity(0.03),
                  backgroundBlendMode: BlendMode.srcOver,
                  shape: BoxShape.circle),
            ),
          ),
          Positioned(
            left: ScreenUtil().screenWidth / 2,
            // left: ScreenUtil().screenWidth * 0.14,
            top: ScreenUtil().screenHeight * 0.01,
            child: SvgPicture.asset(
              'assets/1st_pic_first_tab.svg',
              width: ScreenUtil().screenWidth * 0.35,
              fit: BoxFit.cover,
            ),
          ),
          Positioned(
            top: ScreenUtil().screenHeight / 3.7,
            left: ScreenUtil().screenWidth * 0.15,
            child: Text(
              "1.",
              textAlign: TextAlign.center,
              style: GoogleFonts.lato(
                fontSize: 130,
                fontWeight: FontWeight.w400,
                // letterSpacing: 0.84,
                height: 1.3,
                color: const Color(0xff718096),
              ),
            ),
          ),
          Positioned(
            top: ScreenUtil().screenHeight / 2.99,
            left: ScreenUtil().screenWidth * 0.3,
            child: Text(
              "Erstellen dein Lebenslauf",
              textAlign: TextAlign.start,
              style: GoogleFonts.lato(
                fontSize: 20.sp,
                fontWeight: FontWeight.w600,
                // letterSpacing: 0.84,
                height: 1.3,
                color: const Color(0xff718096),
              ),
            ),
          ),
        ],
      ),
    );
  }

  SizedBox secondStackedImage() {
    return SizedBox(
      // color: Colors.red,
      height: ScreenUtil().screenHeight * 0.57,
      child: Stack(
        children: [
          Positioned(
              top: 0,
              right: 0,
              left: 0,
              // bottom: 75,
              child: RotatedBox(
                quarterTurns: 2,
                child: ClipPath(
                  clipper: WaveClipper(),
                  child: Container(
                    height: ScreenUtil().screenHeight * 0.75,
                    width: ScreenUtil().screenWidth,
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: [Color(0xFFE6FFFA), Color(0xFFEBF4FF)],
                        stops: [0, 1],
                        tileMode: TileMode.repeated,
                      ),
                    ),
                  ),
                ),
              )),
          Positioned(
            top: ScreenUtil().screenHeight * 0.24,
            left: ScreenUtil().screenWidth * 0.55,
            child: Text(
              "2.",
              textAlign: TextAlign.center,
              style: GoogleFonts.lato(
                fontSize: 130,
                fontWeight: FontWeight.w400,
                // letterSpacing: 0.84,
                height: 1.3,
                color: const Color(0xff718096),
              ),
            ),
          ),
          Positioned(
            top: ScreenUtil().screenHeight / 2.7,
            left: ScreenUtil().screenWidth * 0.67,
            child: Text(
              "Erstellen dein Lebenslauf",
              textAlign: TextAlign.start,
              style: GoogleFonts.lato(
                fontSize: 15.75.sp,
                fontWeight: FontWeight.w600,
                letterSpacing: 0.47,
                height: 1.3,
                color: const Color(0xff718096),
              ),
            ),
          ),
          Positioned(
            top: ScreenUtil().screenHeight * 0.22,
            left: ScreenUtil().screenWidth * 0.10,
            child: SvgPicture.asset(
              'assets/2nd_image_first_tab.svg',
              fit: BoxFit.cover,
              width: ScreenUtil().screenWidth * 0.35,
            ),
          ),
        ],
      ),
    );
  }
}
