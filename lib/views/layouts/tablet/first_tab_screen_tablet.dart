import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../widgets/wave_painter.dart';

class FirstTabScreenTablet extends StatelessWidget {
  final double height;
  final double width;
  const FirstTabScreenTablet({
    super.key,
    required this.height,
    required this.width,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Center(
          child: Text(
            "Drei einfache Schritte\nzu deinem neuen Job",
            textAlign: TextAlign.center,
            style: GoogleFonts.lato(
              fontSize: 30.sp,
              fontWeight: FontWeight.w500,
              letterSpacing: 0.94,
              // height: 1.3.h,
              color: const Color(0xff4A5568),
            ),
          ),
        ),
        firstStackedImage(),
        secondStackedImage(),
        thirdStackedImage(),
      ],
    );
  }

  SizedBox thirdStackedImage() {
    return SizedBox(
      // color: Colors.red,
      height: ScreenUtil().screenHeight * 0.62,
      child: Stack(
        children: [
          Positioned(
            top: -ScreenUtil().screenHeight / 8,
            // right: 0,
            left: ScreenUtil().screenWidth * 0.05,
            bottom: 0,
            child: Container(
              width: ScreenUtil().screenWidth * 0.25,
              height: ScreenUtil().screenHeight * 0.25,
              decoration: BoxDecoration(
                  color: Colors.blueGrey.withOpacity(0.03),
                  backgroundBlendMode: BlendMode.srcOver,
                  shape: BoxShape.circle),
            ),
          ),
          Align(
            alignment: const Alignment(-1.1, -0.7),
            child: FractionallySizedBox(
              widthFactor: 0.4,
              heightFactor: 0.4,
              child: Text(
                "3.",
                textAlign: TextAlign.center,
                style: GoogleFonts.lato(
                  fontSize: 130.sp,
                  fontWeight: FontWeight.w400,
                  // letterSpacing: 0.84,
                  height: 1.3,
                  color: const Color(0xff718096),
                ),
              ),
            ),
          ),
          Align(
            alignment: const Alignment(-0.2, -0.15),
            child: FractionallySizedBox(
              widthFactor: 0.4,
              heightFactor: 0.3,
              child: Text(
                "Mit nur einem Klick\nbewerben",
                textAlign: TextAlign.start,
                style: GoogleFonts.lato(
                  fontSize: 23.sp,
                  fontWeight: FontWeight.w600,
                  letterSpacing: 0.47,
                  height: 1.3,
                  color: const Color(0xff718096),
                ),
              ),
            ),
          ),
          Align(
            alignment: const Alignment(0, 1),
            child: FractionallySizedBox(
              widthFactor: 0.7,
              heightFactor: 0.5,
              child: SvgPicture.asset(
                'assets/thrd_image_first_tab.svg',
                fit: BoxFit.contain,
                // width: ScreenUtil().screenWidth * 0.35,
              ),
            ),
          ),
        ],
      ),
    );
  }

  SizedBox firstStackedImage() {
    return SizedBox(
      height: ScreenUtil().screenHeight * 0.75,
      child: Stack(
        children: [
          Positioned(
            top: ScreenUtil().screenHeight * 0.01,
            right: 0,
            left: -ScreenUtil().screenWidth * 1,
            bottom: 0,
            child: FractionallySizedBox(
              widthFactor: 1,
              heightFactor: 1,
              child: Container(
                width: ScreenUtil().screenWidth * 0.45,
                height: ScreenUtil().screenHeight * 0.45,
                decoration: BoxDecoration(
                    color: Colors.blueGrey.withOpacity(0.03),
                    backgroundBlendMode: BlendMode.srcOver,
                    shape: BoxShape.circle),
              ),
            ),
          ),
          Positioned(
            right: 0,
            top: 0,
            left: ScreenUtil().screenWidth * 0.1,
            bottom: ScreenUtil().screenHeight * 0.4,
            child: FractionallySizedBox(
              widthFactor: 0.5,
              heightFactor: 1,
              child: SvgPicture.asset(
                'assets/1st_pic_first_tab.svg',
                fit: BoxFit.contain,
                // width: ScreenUtil().screenWidth * 0.55,
              ),
            ),
          ),
          Align(
            alignment: const Alignment(-1.2, 0),
            child: FractionallySizedBox(
              widthFactor: 0.4,
              heightFactor: 0.3,
              child: Text(
                "1.",
                textAlign: TextAlign.center,
                style: GoogleFonts.lato(
                  fontSize: 130.sp,
                  fontWeight: FontWeight.w400,
                  // letterSpacing: 0.84,
                  height: 1.3,
                  color: const Color(0xff718096),
                ),
              ),
            ),
          ),
          Align(
            alignment: const Alignment(0.1, 0.9),
            child: FractionallySizedBox(
              widthFactor: .5,
              heightFactor: 0.4,
              child: Text(
                "Erstellen dein Lebenslauf",
                textAlign: TextAlign.start,
                style: GoogleFonts.lato(
                  fontSize: 25.sp,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 0.84,
                  height: 1.3,
                  color: const Color(0xff718096),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  SizedBox secondStackedImage() {
    return SizedBox(
      // color: Colors.red,
      height: ScreenUtil().screenHeight * 0.63,
      child: Stack(
        children: [
          Positioned(
              top: 0,
              right: 0,
              left: 0,
              bottom: 0,
              child: RotatedBox(
                quarterTurns: 2,
                child: ClipPath(
                  clipper: WaveClipper(),
                  child: Container(
                    height: ScreenUtil().screenHeight,
                    width: ScreenUtil().screenWidth,
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: [Color(0xFFE6FFFA), Color(0xFFEBF4FF)],
                        stops: [0, 1],
                        tileMode: TileMode.repeated,
                      ),
                    ),
                  ),
                ),
              )),
          Align(
            alignment: const Alignment(-1, -0.65),
            child: FractionallySizedBox(
              widthFactor: 0.4,
              heightFactor: 0.4,
              child: Text(
                "2.",
                textAlign: TextAlign.center,
                style: GoogleFonts.lato(
                  fontSize: 130.sp,
                  fontWeight: FontWeight.w400,
                  // letterSpacing: 0.84,
                  height: 1.3,
                  color: const Color(0xff718096),
                ),
              ),
            ),
          ),
          Align(
            alignment: const Alignment(0.3, 0.23),
            child: FractionallySizedBox(
              widthFactor: .5,
              heightFactor: 0.4,
              child: Text(
                "Erstellen dein Lebenslauf",
                textAlign: TextAlign.start,
                style: GoogleFonts.lato(
                  fontSize: 25.sp,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 0.84,
                  height: 1.3,
                  color: const Color(0xff718096),
                ),
              ),
            ),
          ),
          Align(
            alignment: const Alignment(0, 1),
            child: FractionallySizedBox(
              widthFactor: 0.4,
              heightFactor: 0.5,
              child: SvgPicture.asset(
                'assets/2nd_image_first_tab.svg',
                fit: BoxFit.contain,
                // width: ScreenUtil().screenWidth * 0.55,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
