import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_test_app/views/layouts/widgets/mobile/first_tab_screen_mobile.dart';
import 'package:flutter_test_app/views/layouts/widgets/mobile/second_tab_screen_mobile.dart';
import 'package:flutter_test_app/views/layouts/widgets/mobile/third_tab_screen_mobile.dart';
import 'package:flutter_test_app/widgets/my_button.dart';

import 'package:google_fonts/google_fonts.dart';
// import 'dart:math' as math;

class MobileLayout extends StatefulWidget {
  const MobileLayout({super.key});

  @override
  State<MobileLayout> createState() => _MobileLayoutState();
}

class _MobileLayoutState extends State<MobileLayout>
    with SingleTickerProviderStateMixin {
  double height = 0;
  double width = 0;
  final ScrollController _scrollController = ScrollController();
  var buttonOne = GlobalKey();
  var buttonTwo = GlobalKey();
  var buttonThree = GlobalKey();

  int _currentIndex = 0;
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        height = MediaQuery.of(context).size.height;
        width = MediaQuery.of(context).size.width;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    // _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.white,
      appBar: AppBar(
        toolbarHeight: height * 0.09,
        backgroundColor: Colors.white,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(12),
            bottomRight: Radius.circular(12),
          ),
        ),
        actions: [
          TextButton(
            onPressed: () {},
            child: Text(
              'Login',
              style: GoogleFonts.lato(
                color: const Color(0xff319795),
                fontSize: 14,
                fontWeight: FontWeight.w600,
                letterSpacing: 0.84,
              ),
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView(
              children: [
                Container(
                  height: height * 0.95,
                  width: width,
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      // stops: [140],
                      transform: GradientRotation(2.44346),
                      colors: [
                        Color(0xffEBF4FF),
                        Color(0xffE6FFFA),
                      ],
                    ),
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: height * 0.06,
                      ),
                      Text(
                        'Deine Job \nwebsite',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.lato(
                            fontSize: 42,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1.26,
                            height: 1),
                      ),
                      SvgPicture.asset(
                        'assets/handscheck.svg',
                        fit: BoxFit.cover,
                        height: 418,
                        width: 418,
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                topTabBar(),
                SizedBox(
                  height: height * 0.02,
                ),
                AnimatedSwitcher(
                    duration: const Duration(milliseconds: 500),
                    switchInCurve: Curves.easeIn,
                    switchOutCurve: Curves.easeOut,
                    // reverseDuration: const Duration(milliseconds: 850),
                    transitionBuilder: (child, animation) {
                      return SlideTransition(
                        position: Tween<Offset>(
                                begin: const Offset(1, 0),
                                end: const Offset(0, 0))
                            .animate(animation),
                        child: child,
                      );
                    },
                    layoutBuilder: (currentChild, _) => currentChild!,
                    child: returnCorrectPage()),
              ],
            ),
          ),
          Container(
            height: height * 0.1,
            width: width,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius:
                    const BorderRadius.vertical(top: Radius.circular(12)),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.13),
                      blurRadius: 5,
                      offset: const Offset(0, 0),
                      spreadRadius: 7)
                ]),
            child: Padding(
              padding: EdgeInsets.only(
                top: height * 0.02,
                bottom: height * 0.02,
                right: width * 0.07,
                left: width * 0.07,
              ),
              child: MyElevatedButton(
                borderRadius: BorderRadius.circular(12),
                onPressed: () {},
                child: Text(
                  'Kostenlos Registrieren',
                  style: GoogleFonts.lato(
                      fontSize: 14,
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.84),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget returnCorrectPage() {
    if (_currentIndex == 0) {
      return FirstTabScreen(
        key: const ValueKey<int>(0),
        height: height,
        width: width,
      );
    } else if (_currentIndex == 1) {
      return SecondTabScreen(
        key: const ValueKey<int>(1),
        height: height,
        width: width,
      );
    } else if (_currentIndex == 2) {
      return ThirdTabScreen(
        key: const ValueKey<int>(2),
        height: height,
        width: width,
      );
    } else {
      return FirstTabScreen(
        key: UniqueKey(),
        height: height,
        width: width,
      );
    }
  }

  Container topTabBar() {
    return Container(
      height: ScreenUtil().screenHeight * 0.11,
      width: ScreenUtil().screenWidth * 0.75,
      margin: EdgeInsets.only(left: width * 0.03, right: width * 0.03),
      child: SingleChildScrollView(
        controller: _scrollController,
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            InkWell(
              onTap: () {
                _scrollController.position.ensureVisible(
                  buttonOne.currentContext!.findRenderObject()!,
                  duration: const Duration(milliseconds: 350),
                  curve: Curves.easeInOut,
                );
                // Scrollable.ensureVisible(

                //   buttonOne.currentContext!,

                // );
                setState(() {
                  _currentIndex = 0;
                });
              },
              child: AnimatedContainer(
                key: buttonOne,
                duration: const Duration(milliseconds: 350),
                curve: Curves.easeInBack,
                height: height * 0.05,
                width: width * 0.35,
                decoration: BoxDecoration(
                  color: _currentIndex == 0
                      ? const Color(0xff81E6D9)
                      : Colors.white,
                  borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(12),
                    topLeft: Radius.circular(12),
                  ),
                  border: Border.all(color: const Color(0xffCBD5E0)),
                ),
                child: Center(
                  child: Text(
                    'Arbeitnehmer',
                    style: GoogleFonts.lato(
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.84,
                      color: _currentIndex == 0
                          ? const Color(0xffE6FFFA)
                          : const Color(0xff319795),
                    ),
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                _scrollController.position.ensureVisible(
                  buttonTwo.currentContext!.findRenderObject()!,
                  duration: const Duration(milliseconds: 350),
                  curve: Curves.easeInOut,
                );
                setState(() {
                  _currentIndex = 1;
                });
              },
              child: AnimatedContainer(
                key: buttonTwo,
                duration: const Duration(milliseconds: 350),
                curve: Curves.easeInBack,
                height: height * 0.05,
                width: width * 0.35,
                decoration: BoxDecoration(
                  color: _currentIndex == 1
                      ? const Color(0xff81E6D9)
                      : Colors.white,
                  border: Border.all(color: const Color(0xffCBD5E0)),
                ),
                child: Center(
                  child: Text(
                    'Arbeitgeber',
                    style: GoogleFonts.lato(
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.84,
                      color: _currentIndex == 1
                          ? const Color(0xffE6FFFA)
                          : const Color(0xff319795),
                    ),
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                _scrollController.position.ensureVisible(
                  buttonThree.currentContext!.findRenderObject()!,
                  duration: const Duration(milliseconds: 350),
                  curve: Curves.easeInOut,
                );
                setState(() {
                  _currentIndex = 2;
                });
              },
              child: AnimatedContainer(
                key: buttonThree,
                duration: const Duration(milliseconds: 350),
                curve: Curves.easeInBack,
                height: height * 0.05,
                width: width * 0.35,
                decoration: BoxDecoration(
                  color: _currentIndex == 2
                      ? const Color(0xff81E6D9)
                      : Colors.white,
                  borderRadius: const BorderRadius.only(
                    bottomRight: Radius.circular(12),
                    topRight: Radius.circular(12),
                  ),
                  border: Border.all(color: const Color(0xffCBD5E0)),
                ),
                child: Center(
                  child: Text(
                    'Temporärbüro',
                    style: GoogleFonts.lato(
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.84,
                      color: _currentIndex == 2
                          ? const Color(0xffE6FFFA)
                          : const Color(0xff319795),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
